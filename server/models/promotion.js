const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const mongoosePaginate = require("mongoose-paginate-v2");

const promotionSchema = new Schema({
  name: {
    type: String,
  },
  type: {
    type: String,
  },
  startDate:{
	type: Date,
  },
  endDate:{
	type: Date,
  },
  userGroupName:{
	type: String
  }
});

promotionSchema.plugin(mongoosePaginate);
const promotion = mongoose.model('promotion', promotionSchema);

module.exports = promotion;
